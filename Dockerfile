FROM alpine:3.4

ENV PGWEB_VERSION 0.9.4

RUN apk --no-cache add curl unzip && \
    curl -L -o /tmp/pgweb_linux_amd64.zip https://github.com/sosedoff/pgweb/releases/download/v$PGWEB_VERSION/pgweb_linux_amd64.zip && \
    unzip /tmp/pgweb_linux_amd64.zip -d /usr/bin && \
    mv /usr/bin/pgweb_linux_amd64 /usr/bin/pgweb && \
    rm -f pgweb_linux_amd64.zip

EXPOSE 8081
CMD ["/usr/bin/pgweb", "--bind=0.0.0.0", "--listen=8081"]